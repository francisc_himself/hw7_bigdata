#!/usr/bin/python
from PIL import Image, ImageDraw
from sklearn.cluster import KMeans, DBSCAN
import argparse
import sys
from sklearn.preprocessing import StandardScaler
from sklearn import metrics

import numpy as np
from pprint import pprint

imageCircles = Image.open("dataset/circles.png")
imageFull    = Image.open("dataset/full.png")
imageMoons   = Image.open("dataset/moons.png")
imageSpots   = Image.open("dataset/spots.png")
imageStripes = Image.open("dataset/stripes.png")
imagesList = [imageCircles, imageFull, imageMoons, imageSpots, imageStripes]

colorsList = [
(255, 0, 0),#BLUE
(50, 255, 50),#GREEN
(0, 0, 255),#RED
(255, 0, 255),#MAGENTA
(247, 131, 30),#DARK TURQOUISE
(0, 242, 255),#YELLOW
(98, 156, 38),#DARK GREEN
]
############## GLOBALS ###########################
processedImage = imageCircles
processedWidth = 0
processedHeight = 0
############## GLOBALS ###########################

##################################################
def imgToList(image):
    listOfCoords = []
    width, height = image.size
    for idx_i in range(width):
        for idx_j in range(height):
            if (image.getpixel((idx_i, idx_j)) == 0):
                listOfCoords.append([idx_i, idx_j])
    return listOfCoords
##################################################


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # K-Means Demo # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def k_means_demo(processedImage, numberOfClusters):
    processedWidth, processedHeight = processedImage.size
    # 1) Fill the coordinates list
    listCoords = imgToList(processedImage)
    # 2) Call the KMeans class constructor
    kmeans = KMeans(n_clusters=numberOfClusters, random_state=1).fit(listCoords)
    radius = 5
    processedImage.show()
    # 4) Draw a new image, make sure it is RGB
    imageResultColoured = Image.new('RGB', processedImage.size, (255,255,255))
    # Counter below counts pixels detected
    counter_pixels_hit = 0
    for idx_i in range (processedWidth):
        for idx_j in range (processedHeight):
            # Check if B/W pixel is black
            if (processedImage.getpixel((idx_i, idx_j)) == 0):
                counter_pixels_hit+=1
                if kmeans.labels_[counter_pixels_hit-1] == 0:
                    imageResultColoured.putpixel((idx_i, idx_j), colorsList[0])
                else:
                    if kmeans.labels_[counter_pixels_hit-1] == 1:
                        imageResultColoured.putpixel((idx_i, idx_j), colorsList[1])
                    else:
                        if kmeans.labels_[counter_pixels_hit-1] == 2:
                            imageResultColoured.putpixel((idx_i, idx_j), colorsList[2])
                        else:
                            if kmeans.labels_[counter_pixels_hit - 1] == 3:
                                imageResultColoured.putpixel((idx_i, idx_j), colorsList[3])
                            else:
                                if kmeans.labels_[counter_pixels_hit - 1] == 4:
                                    imageResultColoured.putpixel((idx_i, idx_j), colorsList[4])
                                else:
                                    if kmeans.labels_[counter_pixels_hit - 1] == 5:
                                        imageResultColoured.putpixel((idx_i, idx_j), colorsList[5])
                                    else:
                                        if kmeans.labels_[counter_pixels_hit - 1] == 6:
                                            imageResultColoured.putpixel((idx_i, idx_j), colorsList[6])

    for idx, a_center in enumerate(kmeans.cluster_centers_):
        print "Drawing #", idx
        x_test, y_test = kmeans.cluster_centers_[idx]
        draw = ImageDraw.Draw(imageResultColoured)
        draw.ellipse((x_test-radius, y_test-radius, x_test+radius, y_test+radius), fill= (15, 20, 200))
    print "HIT", counter_pixels_hit
    imageResultColoured.show()
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # K-Means Demo # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # DBSCAN Demo # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def dbscan_demo(processedImage):
    print "DBSCAN"
    processedWidth, processedHeight = processedImage.size
    # 1) Fill the coordinates list
    listCoords = imgToList(processedImage)
    listCoords = StandardScaler().fit_transform(listCoords)
    # 2) Compute DBSCAN
    db = DBSCAN(eps=0.09, min_samples=10).fit(listCoords)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_

    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

    print('Estimated number of clusters: %d' % n_clusters_)
    radius = 5
    # 4) Draw a new image, make sure it is RGB
    imageResultColoured = Image.new('RGB', processedImage.size, (255, 255, 255))
    # Counter below counts pixels detected
    counter_pixels_hit = 0
    for idx_i in range(processedWidth):
        for idx_j in range(processedHeight):
            # Check if B/W pixel is black
            if (processedImage.getpixel((idx_i, idx_j)) == 0):
                counter_pixels_hit += 1
                if db.labels_[counter_pixels_hit - 1] == 0:
                    imageResultColoured.putpixel((idx_i, idx_j), colorsList[0])
                else:
                    if db.labels_[counter_pixels_hit - 1] == 1:
                        imageResultColoured.putpixel((idx_i, idx_j), colorsList[1])
                    else:
                        if db.labels_[counter_pixels_hit - 1] == 2:
                            imageResultColoured.putpixel((idx_i, idx_j), colorsList[2])
                        else:
                            if db.labels_[counter_pixels_hit - 1] == 3:
                                imageResultColoured.putpixel((idx_i, idx_j), colorsList[3])
                            else:
                                if db.labels_[counter_pixels_hit - 1] == 4:
                                    imageResultColoured.putpixel((idx_i, idx_j), colorsList[4])
                                else:
                                    if db.labels_[counter_pixels_hit - 1] == 5:
                                        imageResultColoured.putpixel((idx_i, idx_j), colorsList[5])
                                    else:
                                        if db.labels_[counter_pixels_hit - 1] == 6:
                                            imageResultColoured.putpixel((idx_i, idx_j), colorsList[6])

    imageResultColoured.show()
    # print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels_true, labels))
    # print("Completeness: %0.3f" % metrics.completeness_score(labels_true, labels))
    # print("V-measure: %0.3f" % metrics.v_measure_score(labels_true, labels))
    # print("Adjusted Rand Index: %0.3f"
    #       % metrics.adjusted_rand_score(labels_true, labels))
    # print("Adjusted Mutual Information: %0.3f"
    #       % metrics.adjusted_mutual_info_score(labels_true, labels))
    # print("Silhouette Coefficient: %0.3f"
    #       % metrics.silhouette_score(X, labels))
    pass
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # DBSCAN Demo # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


################Main#####################
parser = argparse.ArgumentParser(description="HW7 BIG DATA")
parser.add_argument("-kmeans", action='store_true', help="K-means Clustering", required=False)
parser.add_argument("-dbscan", action='store_true', help="K-means Clustering", required=False)
parser.add_argument("image_number", type=int, help="Number from 0 to 4 in this example")
parser.add_argument("number_of_clusters", type=int, help="K-means: input number of desired clusters")
args = parser.parse_args()
if len(sys.argv[1:])==0:
    print "MAAAAAAI"
    parser.print_help()

if args.kmeans:
    if args.image_number:
        print "Image Number=", args.image_number
        processedImage = imagesList[args.image_number-1]
        if args.number_of_clusters:
            k_means_demo(processedImage, args.number_of_clusters)
        else:
            k_means_demo(processedImage, 2)
else:
    if args.dbscan:
        if args.image_number:
            processedImage = imagesList[args.image_number - 1]
            dbscan_demo(processedImage)