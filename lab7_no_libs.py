#!/usr/bin/python
from PIL import Image, ImageDraw

from __builtin__ import list
from sklearn.cluster import KMeans, DBSCAN
import argparse
import sys
from sklearn.preprocessing import StandardScaler
from sklearn import metrics
import numpy as np
from pprint import pprint
from random import randint
import  scipy.stats



imageCircles = Image.open("dataset/circles.png")
imageFull    = Image.open("dataset/full.png")
imageMoons   = Image.open("dataset/moons.png")
imageSpots   = Image.open("dataset/spots.png")
imageStripes = Image.open("dataset/stripes.png")
imagesList = [imageCircles, imageFull, imageMoons, imageSpots, imageStripes]

colorsList = [
(255, 0, 0),#BLUE
(50, 255, 50),#GREEN
(0, 0, 255),#RED
(255, 0, 255),#MAGENTA
(247, 131, 30),#DARK TURQOUISE
(0, 242, 255),#YELLOW
(98, 156, 38),#DARK GREEN
]

############## GLOBALS ###########################
processedImage = imageCircles
processedWidth = 0
processedHeight = 0
centroids = []
g_labels = []
MAX_ITERATIONS = 5
############## GLOBALS ###########################

##################################################
def imgToList(image):
    listOfCoords = []
    width, height = image.size
    for idx_i in range(width):
        for idx_j in range(height):
            if (image.getpixel((idx_i, idx_j)) == 0):
                listOfCoords.append([idx_i, idx_j])
    return listOfCoords
##################################################

# Function Get Random Centroids
# -----------------------------
# returns k tuples of coordinates
# randomly selected from the image
def getRandomCentroids(image, k):
    w, h = image.size
    retCentroids = []
    for idx in range(k):
        randWidth, randHeight = randint(0, w), randint(0, h)
        retCentroids.append([randWidth, randHeight])
    return retCentroids

# Function: Get Centroids
# -----------------------
# Returns k centroids, recalculated
def getCentroids(dataSet, labels, k):
    # Each centroid is the geometric mean of the points that have
    # for idx, a_label in enumerate(labels):
    dict_lists_by_label = dict()

    dict_of_lists = dict()
    for idx, a_label in enumerate(labels):
        if a_label not in dict_of_lists:
            dict_of_lists[a_label] = [dataSet[idx]]
        else:
            dict_of_lists[a_label].append(dataSet[idx])

    dict_of_means = dict() # this dict has means for each centroid
    list_centroids = []
    for a_label in dict_of_lists:
        x = scipy.stats.gmean(dict_of_lists[a_label])
        dict_of_means[a_label] = x
        list_centroids.append([x[0],x[1]])
    return list_centroids

# Function: Should stop
# ---------------------
# Returns True or False if k-means is done. K-means terminates
# either because it has run a maximum number of times, OR the
# centroids stop changing
def shouldStop(oldCentroids, centroids, iterations):
    if iterations >= MAX_ITERATIONS: return True
    # return oldCentroids == centroids

# Function: Get Labels
# --------------------
# Returns a label for each piece of data in the dataset
def getLabels(dataSet, centroids):
    # For each element in the dataset, choose the closest centroid
    # Make that centroid the element's label
    labels = []
    for idx_elem, an_element in enumerate(dataSet):
        x_elem = an_element[0]
        y_elem = an_element[1]
        min_leads_to = 0  # cluster 0 is closest so far
        min_sofar = 0
        for idx_centr, a_centroid in enumerate(centroids):
            x_centroid = a_centroid[0]
            y_centroid = a_centroid[1]
            dist_squared = (y_centroid-y_elem)*(y_centroid-y_elem)+(x_centroid-x_elem)*(x_centroid-x_elem)
            if idx_centr == 0:
                min_sofar = dist_squared
                min_leads_to = idx_centr
            else:
                if dist_squared < min_sofar:
                    min_sofar = dist_squared
                    min_leads_to = idx_centr
        labels.append(min_leads_to)
    return labels


# Function: K Means
# -----------------
# K-Means is an algorithm that takes in a dataset and a constant
# k and returns k centroids (which define clusters of data in the
# dataset which are similar to one another).
def kmeans(dataSet, k):
    # 1) Initializations for centroids
    centroids = getRandomCentroids(processedImage, k)
    print "RANDOM CENTROIDS"
    pprint(centroids)

    # 2) Draw a new image, make sure it is RGB
    img_list = []

    # 4) Initializations for Bookkeeping:
    iterations = 0
    oldCentroids = []

    # Run the k-means algo
    while not shouldStop(oldCentroids, centroids, iterations):

        imageResultColoured = Image.new('RGB', processedImage.size, (255, 255, 255))
        draw = ImageDraw.Draw(imageResultColoured)

        # Save old centroids for convergence test. Book keeping
        oldCentroids = centroids
        iterations += 1

        # Assign labels to each datapoint based on centroids
        labels = getLabels(dataSet, centroids)

        print "labels len=", len(labels)
        print "image size=", processedImage.size
        processedWidth, processedHeight = processedImage.size

        counter_pixels_hit = 0
        for idx_i in range(processedWidth):
            for idx_j in range(processedHeight):
                if (processedImage.getpixel((idx_i, idx_j)) == 0):
                    counter_pixels_hit+=1
                    imageResultColoured.putpixel((idx_i, idx_j), colorsList[labels[counter_pixels_hit-1]])

        # 3) Illustrate the initial centroids in the canvas
        radius = 4
        for idx, a_centroid in enumerate(centroids):
            draw.ellipse((a_centroid[0] - radius, a_centroid[1] - radius, \
                          a_centroid[0] + radius, a_centroid[1] + radius), \
                         fill='yellow', outline='black')

        # Assign centroids based on datapoint labels

        someCentroids = getCentroids(dataSet, labels, k)
        print ("SOME CENTROIDS:")
        pprint (someCentroids)


        # for idx, a_centroid in enumerate(someCentroids):
        #     its_x, its_y = someCentroids[a_centroid][0], someCentroids[a_centroid][1]
        #     draw.ellipse((its_x - radius, its_y - radius, \
        #                   its_x + radius, its_y + radius), \
        #                  fill='orange', outline='black')

        for idx, a_centroid in enumerate(someCentroids):
            draw.ellipse((a_centroid[0] - radius, a_centroid[1] - radius, \
                          a_centroid[0] + radius, a_centroid[1] + radius), \
                         fill='orange', outline='black')

        centroids = someCentroids

        imageResultColoured.show()
    print "iterations=", iterations

    pass
### Main ###
listCoords = imgToList(processedImage)

kmeans(listCoords, 4)